#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUF_SIZE 16
#include "aes.h"

int
main() {

    unsigned char* plain    = malloc(BUF_SIZE*sizeof(char));
    unsigned char* cipher   = malloc(BUF_SIZE*sizeof(char));
    unsigned char* key      = malloc(BUF_SIZE*sizeof(char));

    memset(cipher, 0, BUF_SIZE);
    sprintf(plain, "%s", "thisisaplaintext");
    sprintf(key, "%s", "0123456789ABCDEF");

    while (1) {
        AESEncrypt(cipher, plain, key);
        printf("%s\n", cipher);
    }
    return 0;
}

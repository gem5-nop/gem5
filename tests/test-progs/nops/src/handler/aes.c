/**
* @Author: hélène, ronan
* @Date:   12-09-2016
* @Email:  helene.lebouder@inri.fr, ronan.lashermes@inria.fr
* @Last modified by:   ronan
* @Last modified time: 19-09-2016
* @License: GPL
*/

#include "aes.h"

//----------------------------------------------------------
// AES128 encryption
//----------------------------------------------------------
void AESEncrypt(unsigned char* ciphertext, unsigned char* plaintext,
        unsigned char* key)
{
        unsigned char master_key[STATE_ROW_SIZE][STATE_ROW_SIZE];
        unsigned char state [STATE_ROW_SIZE][STATE_ROW_SIZE];
        unsigned char round_key[STATE_ROW_SIZE][STATE_ROW_SIZE];
        unsigned char keys[ROUND_COUNT+1][STATE_ROW_SIZE][STATE_ROW_SIZE];
        int round,i,j;

        //shaping key
        MessageToState(master_key, key);
        //plaintext to state
        MessageToState(state, plaintext);
        //key generation
        KeyGen(keys, master_key);


        // xor key
        AddRoundKey( state, master_key);


        for (round=1;round<ROUND_COUNT;round++)
        {
                // SB
                if (trigger_sel == round) {
                        SubBytes(state);
                } else {
                        SubBytes(state);
                }

                // SR
                ShiftRows(state);
                // MC
                MixColumns(state);

                //  xor key
                GetRoundKey(round_key,keys,round);
                AddRoundKey( state, round_key);
        }


        // SB
        if (trigger_sel == round) {
                SubBytes(state);
        } else {
                SubBytes(state);
        }

        // SR
        ShiftRows(state);

        // xor key
        GetRoundKey(round_key,keys,ROUND_COUNT);
        AddRoundKey( state, round_key);

        //ecriture du message en ligne
        StateToMessage(ciphertext,state);

        return;

}

//----------------------------------------------------------
// AES Round functions
//----------------------------------------------------------
void SubBytes(unsigned char state[][STATE_ROW_SIZE])
{
        state[0][0]=sboxtab[state[0][0]];
        state[0][1]=sboxtab[state[0][1]];
        state[0][2]=sboxtab[state[0][2]];
        state[0][3]=sboxtab[state[0][3]];

        state[1][0]=sboxtab[state[1][0]];
        state[1][1]=sboxtab[state[1][1]];
        state[1][2]=sboxtab[state[1][2]];
        state[1][3]=sboxtab[state[1][3]];

        state[2][0]=sboxtab[state[2][0]];
        state[2][1]=sboxtab[state[2][1]];
        state[2][2]=sboxtab[state[2][2]];
        state[2][3]=sboxtab[state[2][3]];

        state[3][0]=sboxtab[state[3][0]];
        state[3][1]=sboxtab[state[3][1]];
        state[3][2]=sboxtab[state[3][2]];
        state[3][3]=sboxtab[state[3][3]];
}


// fonction SR
void ShiftRows(unsigned char state[][STATE_ROW_SIZE])
{
        unsigned char temp;
        //shif row 0 do nothing

        //shift row 1
        temp = state[1][0];
        state[1][0]= state[1][1];
        state[1][1]= state[1][2];
        state[1][2]= state[1][3];
        state[1][3]= temp;

        //shift row 2
        temp = state[2][0];
        state[2][0]= state[2][2];
        state[2][2]= temp;
        temp = state[2][1];
        state[2][1]= state[2][3];
        state[2][3]= temp;

        //shift row 3
        temp = state[3][0];
        state[3][0]= state[3][3];
        state[3][3]= state[3][2];
        state[3][2]= state[3][1];
        state[3][1]= temp;

        return;

}

// fonction MC
void MixColumns(unsigned char state [][STATE_ROW_SIZE])
{
        int i,j;
        unsigned char column [STATE_ROW_SIZE];
        for (j=0;j<STATE_ROW_SIZE;j++)
                {
                        for (i=0;i<STATE_ROW_SIZE;i++)
                        {
                                column[i]=state[i][j];
                        }

                        MCMatrixColumnProduct(column);

                        for (i=0;i<STATE_ROW_SIZE;i++)
                        {
                                state[i][j]=column[i];
                        }
                }

        return;
}


//----------------------------------------------------------
// Génération de clé
//----------------------------------------------------------
// round keys generation
void KeyGen(unsigned char keys [][STATE_ROW_SIZE][STATE_ROW_SIZE],
        unsigned char master_key [][STATE_ROW_SIZE])
{
        int round,row,col;
        int i,j,k;


        for (row=0;row<STATE_ROW_SIZE;row++)
        {
                for (col=0;col<STATE_ROW_SIZE;col++)
                {
                        keys[0][row][col]=master_key[row][col];
                }
        }

        for (round=1;round<ROUND_COUNT+1;round++)
        {
                ColumnFill(keys,round);
                OtherColumnsFill(keys,round);
        }

        return;
}

//Creation of first column of a round key
void ColumnFill(unsigned char keys[][STATE_ROW_SIZE][STATE_ROW_SIZE],int round)
{
        int i;
        unsigned char column [STATE_ROW_SIZE];
        //rotate
        column[0]=keys[round-1][1][3];
        column[1]=keys[round-1][2][3];
        column[2]=keys[round-1][3][3];
        column[3]=keys[round-1][0][3];


        //sbox + rcon + xor
        keys[round][0][0] =
            sboxtab[column[0]] ^ rcon[round-1] ^ keys[round-1][0][0];
        for (i=1;i<STATE_ROW_SIZE;i++)
        {
                keys[round][i][0] = sboxtab[column[i]] ^ keys[round-1][i][0];
        }
        return;
}

//Creation of other round key columns
void OtherColumnsFill(unsigned char keys[][STATE_ROW_SIZE][STATE_ROW_SIZE],
        int round)
{
        int row,col;
        for (col=1;col<STATE_ROW_SIZE;col++) //column 1 to 3
        {
                for (row=0;row<STATE_ROW_SIZE;row++)
                {
                        keys[round][row][col] =
                            keys[round-1][row][col] ^ keys[round][row][col-1];
                }
        }

        return;
}

//Get selected round key
void GetRoundKey(unsigned char round_key [][STATE_ROW_SIZE],
        unsigned char keys [][STATE_ROW_SIZE][STATE_ROW_SIZE], int round)
{
        int i,j;
        for (i=0;i<STATE_ROW_SIZE;i++)
        {
                for (j=0;j<STATE_ROW_SIZE;j++)
                {
                        round_key[i][j]=keys[round][i][j];
                }
        }
        return;
}

//----------------------------------------------------------
// Other useful functions
//----------------------------------------------------------
// Transform a vector into a state matrix
void MessageToState(unsigned char state [][STATE_ROW_SIZE],
        unsigned char message [])
{
        int i,j;
        for (i=0;i<STATE_ROW_SIZE;i++)
        {
                for (j=0;j<STATE_ROW_SIZE;j++)
                {
                        state[i][j]=message[j*STATE_ROW_SIZE+i];
                }
        }
        return;
}

// Transform a state matrix into a vector
void StateToMessage(unsigned char message[],
        unsigned char state [][STATE_ROW_SIZE])
{
        int i,j;
        for (i=0;i<STATE_ROW_SIZE;i++)
        {
                for (j=0;j<STATE_ROW_SIZE;j++)
                {
                        message[j*STATE_ROW_SIZE+i]=state[i][j];
                }
        }
        return;
}



//for each element arr1[i] = arr1[i] xor arr2[i]
void AddRoundKey(unsigned char arr1 [][STATE_ROW_SIZE],
        unsigned char arr2 [][STATE_ROW_SIZE])
{
        int i,j;
        for (i=0;i<STATE_ROW_SIZE;i++)
        {
                for (j=0;j<STATE_ROW_SIZE;j++)
                {
                        arr1[i][j] = arr1[i][j] ^ arr2[i][j];
                }
        }
        return;
}


//MC Matrix product (see specs)
void MCMatrixColumnProduct(unsigned char column [])
{
        int i;
        unsigned char column_tmp  [STATE_ROW_SIZE];
        //copy column
        for (i=0; i<STATE_ROW_SIZE; i++) {
                column_tmp[i] = column[i];
        }

        column[0]= gmul(column_tmp[0],2) ^ gmul(column_tmp[3],1) ^
                   gmul(column_tmp[2],1) ^ gmul(column_tmp[1],3);
        column[1]= gmul(column_tmp[0],1) ^ gmul(column_tmp[1],2) ^
                   gmul(column_tmp[2],3) ^ gmul(column_tmp[3],1);
        column[2]= gmul(column_tmp[0],1) ^ gmul(column_tmp[1],1) ^
                   gmul(column_tmp[2],2) ^ gmul(column_tmp[3],3);
        column[3]= gmul(column_tmp[0],3) ^ gmul(column_tmp[1],1) ^
                   gmul(column_tmp[2],1) ^ gmul(column_tmp[3],2);

        return;
}

//Galois field product
unsigned char gmul(unsigned char a, unsigned char b)
{
        unsigned char p = 0;
        unsigned char beacon = 0;
        unsigned char counter, hi_bit_set;
        for (counter = 0; counter < 8; counter++)
        {
                if ((b & 1) == 1)
                        p ^= a;
                else
                        beacon ^= a;


                hi_bit_set = (a & 0x80);
                a <<= 1;
                if (hi_bit_set == 0x80)
                        a ^= 0x1b;
                else
                        beacon ^= 0x1b;

                b >>= 1;
        }
        return p;
}


//----------------------------------------------------------
// Tables
//----------------------------------------------------------
unsigned char trigger_sel = 1;

const unsigned char rcon [10]={0x01 , 0x02, 0x04, 0x08, 0x10,
                               0x20, 0x40, 0x80, 0x1b, 0x36  };

const unsigned char sboxtab [256]={
    0x63, 0x7C, 0x77, 0x7B, 0xF2, 0x6B, 0x6F, 0xC5,
    0x30, 0x01, 0x67, 0x2B, 0xFE, 0xD7, 0xAB, 0x76,
    0xCA, 0x82, 0xC9, 0x7D, 0xFA, 0x59, 0x47, 0xF0,
    0xAD, 0xD4, 0xA2, 0xAF, 0x9C, 0xA4, 0x72, 0xC0,
    0xB7, 0xFD, 0x93, 0x26, 0x36, 0x3F, 0xF7, 0xCC,
    0x34, 0xA5, 0xE5, 0xF1, 0x71, 0xD8, 0x31, 0x15,
    0x04, 0xC7, 0x23, 0xC3, 0x18, 0x96, 0x05, 0x9A,
    0x07, 0x12, 0x80, 0xE2, 0xEB, 0x27, 0xB2, 0x75,
    0x09, 0x83, 0x2C, 0x1A, 0x1B, 0x6E, 0x5A, 0xA0,
    0x52, 0x3B, 0xD6, 0xB3, 0x29, 0xE3, 0x2F, 0x84,
    0x53, 0xD1, 0x00, 0xED, 0x20, 0xFC, 0xB1, 0x5B,
    0x6A, 0xCB, 0xBE, 0x39, 0x4A, 0x4C, 0x58, 0xCF,
    0xD0, 0xEF, 0xAA, 0xFB, 0x43, 0x4D, 0x33, 0x85,
    0x45, 0xF9, 0x02, 0x7F, 0x50, 0x3C, 0x9F, 0xA8,
    0x51, 0xA3, 0x40, 0x8F, 0x92, 0x9D, 0x38, 0xF5,
    0xBC, 0xB6, 0xDA, 0x21, 0x10, 0xFF, 0xF3, 0xD2,
    0xCD, 0x0C, 0x13, 0xEC, 0x5F, 0x97, 0x44, 0x17,
    0xC4, 0xA7, 0x7E, 0x3D, 0x64, 0x5D, 0x19, 0x73,
    0x60, 0x81, 0x4F, 0xDC, 0x22, 0x2A, 0x90, 0x88,
    0x46, 0xEE, 0xB8, 0x14, 0xDE, 0x5E, 0x0B, 0xDB,
    0xE0, 0x32, 0x3A, 0x0A, 0x49, 0x06, 0x24, 0x5C,
    0xC2, 0xD3, 0xAC, 0x62, 0x91, 0x95, 0xE4, 0x79,
    0xE7, 0xC8, 0x37, 0x6D, 0x8D, 0xD5, 0x4E, 0xA9,
    0x6C, 0x56, 0xF4, 0xEA, 0x65, 0x7A, 0xAE, 0x08,
    0xBA, 0x78, 0x25, 0x2E, 0x1C, 0xA6, 0xB4, 0xC6,
    0xE8, 0xDD, 0x74, 0x1F, 0x4B, 0xBD, 0x8B, 0x8A,
    0x70, 0x3E, 0xB5, 0x66, 0x48, 0x03, 0xF6, 0x0E,
    0x61, 0x35, 0x57, 0xB9, 0x86, 0xC1, 0x1D, 0x9E,
    0xE1, 0xF8, 0x98, 0x11, 0x69, 0xD9, 0x8E, 0x94,
    0x9B, 0x1E, 0x87, 0xE9, 0xCE, 0x55, 0x28, 0xDF,
    0x8C, 0xA1, 0x89, 0x0D, 0xBF, 0xE6, 0x42, 0x68,
    0x41, 0x99, 0x2D, 0x0F, 0xB0, 0x54, 0xBB, 0x16};


const unsigned char invsbox [256]={
    0x52, 0x09, 0x6A, 0xD5, 0x30, 0x36, 0xA5, 0x38,
    0xBF, 0x40, 0xA3, 0x9E, 0X81, 0xF3, 0xD7, 0xFB,
    0x7C, 0xE3, 0x39, 0x82, 0x9B, 0x2F, 0xFF, 0x87,
    0x34, 0x8E, 0x43, 0x44, 0xC4, 0xDE, 0xE9, 0xCB,
    0x54, 0x7B, 0x94, 0x32, 0xA6, 0xC2, 0x23, 0x3D,
    0xEE, 0x4C, 0x95, 0x0B, 0x42, 0xFA, 0xC3, 0x4E,
    0x08, 0x2E, 0xA1, 0x66, 0x28, 0xD9, 0x24, 0xB2,
    0x76, 0x5B, 0xA2, 0x49, 0x6D, 0x8B, 0xD1, 0x25,
    0x72, 0xF8, 0xF6, 0x64, 0x86, 0x68, 0x98, 0x16,
    0xD4, 0xA4, 0x5C, 0xCC, 0x5D, 0x65, 0xB6, 0x92,
    0x6C, 0x70, 0x48, 0x50, 0xFD, 0xED, 0xB9, 0xDA,
    0x5E, 0x15, 0x46, 0x57, 0xA7, 0x8D, 0x9D, 0x84,
    0x90, 0xD8, 0xAB, 0x00, 0x8C, 0xBC, 0xD3, 0x0A,
    0xF7, 0xE4, 0x58, 0x05, 0xB8, 0xB3, 0x45, 0x06,
    0xD0, 0x2C, 0x1E, 0x8F, 0xCA, 0x3F, 0x0F, 0x02,
    0xC1, 0xAF, 0xBD, 0x03, 0x01, 0x13, 0x8A, 0x6B,
    0x3A, 0x91, 0x11, 0x41, 0x4F, 0x67, 0xDC, 0xEA,
    0x97, 0xF2, 0xCF, 0xCE, 0xF0, 0xB4, 0xE6, 0x73,
    0x96, 0xAC, 0x74, 0x22, 0xE7, 0xAD, 0x35, 0x85,
    0xE2, 0xF9, 0x37, 0xE8, 0x1C, 0x75, 0xDF, 0x6E,
    0x47, 0xF1, 0x1A, 0x71, 0x1D, 0x29, 0xC5, 0x89,
    0x6F, 0xB7, 0x62, 0x0E, 0xAA, 0x18, 0xBE, 0x1B,
    0xFC, 0x56, 0x3E, 0x4B, 0xC6, 0xD2, 0x79, 0x20,
    0x9A, 0xDB, 0xC0, 0xFE, 0x78, 0xCD, 0x5A, 0xF4,
    0x1F, 0xDD, 0xA8, 0x33, 0x88, 0x07, 0xC7, 0x31,
    0xB1, 0x12, 0x10, 0x59, 0x27, 0x80, 0xEC, 0x5F,
    0x60, 0x51, 0x7F, 0xA9, 0x19, 0xB5, 0x4A, 0x0D,
    0x2D, 0xE5, 0x7A, 0x9F, 0x93, 0xC9, 0x9C, 0xEF,
    0xA0, 0xE0, 0x3B, 0x4D, 0xAE, 0x2A, 0xF5, 0xB0,
    0xC8, 0xEB, 0xBB, 0x3C, 0x83, 0x53, 0x99, 0x61,
    0x17, 0x2B, 0x04, 0x7E, 0xBA, 0x77, 0xD6, 0x26,
    0xE1, 0x69, 0x14, 0x63, 0x55, 0x21, 0x0C, 0x7D };

# End of the attack

The following programs crash when the attack is finished:
* secret_layout
* secret_handler
* secret_generic_1
* secret_hello with `nops-crash.txt`

The following programs enter in an infinite loop:
* secret_generic_2
* secret_hello with `nops-loop.txt`

These behaviors are expected. We leave the CPU registers with non-sense values.


# Execution time

Each use-case takes only few secondes to complete, except the *Hello World!*. This one requires few minutes to complete. With an Intel i7-8650U CPU at 1.90MHz, it is around 10 minutes.


# Hello World!

To see the current status of the attack, comment the line which contains the address 0x105a8. This address is the call to `printf()` in the main function. By doing so, the output string is printed after each new added character.

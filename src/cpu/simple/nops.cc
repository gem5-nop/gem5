/*
 * Copyright (c) 2020, French Institute for Research in Computer Science and
 * Automation
 * Author: Pierre-Yves Peneau <first.last@inria.fr>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "cpu/simple/nops.hh"

#include <algorithm>
#include <fstream>

#include "debug/NopsSimpleCPU.hh"
#include "debug/NopsSimpleCPUExec.hh"
#include "debug/NopsSimpleCPUInit.hh"

using namespace std;
using namespace TheISA;

NopsSimpleCPU::NopsSimpleCPU(NopsSimpleCPUParams *p)
    : TimingSimpleCPU(p),
      synchro_reached(false),
      synchro_addr(0),
      synchro_exec(1),
      nopsFile(p->nops_file)
{

    std::ifstream file(nopsFile);
    Addr addr;

    if (file.is_open()) {

        std::string line;

        // Ignore comments
        while (getline(file, line)) {
            if (line.at(0) == '#') {
                continue;
            } else {
                break;
            }
        }

        // First line always contains the synchronisation point and optionally
        // the number of normal execution for this instruction
        std::stringstream tmp;
        tmp << std::hex << split_str(line, 0);
        tmp >> synchro_addr;
        tmp.clear();

        if ( count_words(line) > 1 ) {
            synchro_exec = std::stoi(split_str(line, 1));
        }

        DPRINTF(NopsSimpleCPUInit, "Entry %x e:%d\n",
                synchro_addr, synchro_exec);

        // Now, we read the entire file line by line
        while (std::getline(file, line)) {

            // Ignore commented line
            if (line.at(0) == '#') {
                continue;
            }

            // Extract the address
            tmp << std::hex << split_str(line, 0);
            tmp >> addr;
            tmp.clear();

            // For each new line, we need to create a queue Q to store actions
            std::queue< std::pair<char, long long int>*>* Q =
                new std::queue<std::pair<char, long long int>*>;

            if ( count_words(line) == 1 ) {
                DPRINTF(NopsSimpleCPUInit, "%x -> s:infinite\n", addr);
            }

            std::string word;
            // While there is an action to do (skip address at position 0)
            for ( int i = 1; i < count_words(line); i++ ) {

                // Extract word, i.e., action:number
                word = split_str(line, i);

                // For each action we need to create a pair P
                std::pair<char, long long int>* P =
                    new std::pair<char, long long int>;

                // Extract action and number
                P->first  = word.at(0);
                P->second = std::stoi(word.erase(0, 2));

                DPRINTF(NopsSimpleCPUInit, "%x -> %c:%d\n",
                        addr, P->first, P->second);

                // Insert this pair P into the queue
                Q->push(P);
            }

            // Insert the queue in the map with the address as the key
            skip_list.insert({addr, Q});
        }

        // Close file
        file.close();
    }
}

NopsSimpleCPU::~NopsSimpleCPU()
{
}

/* NOTES:
 *  - This implementation does not support multithread (should we ?)
 *  - I need to pay attention to the updateCycle<..>() functions. With or
 *    without comment, the finale cycle is the same, but they could have side
 *    effects. I need to read the code
*/
void
NopsSimpleCPU::fetch()
{
    // Change thread if multi-threaded (FIXME)
    //swapActiveThread();

    SimpleExecContext &t_info = *threadInfo[curThread];
    SimpleThread* thread = t_info.thread;

    if (!curStaticInst || !curStaticInst->isDelayedCommit()) {
        checkForInterrupts();
        checkPcEventQueue();
    }

    // We must have just got suspended by a PC event
    if (_status == Idle)
        return;

    TheISA::PCState pcState = thread->pcState();
    bool needToFetch = !isRomMicroPC(pcState.microPC()) &&
                       !curMacroStaticInst;

    if (needToFetch) {

        // Which address has to be fetched ?
        Addr fetchPC = (thread->instAddr() & PCMask) + t_info.fetchOffset;

        // Before anything, we need to reach the synchronisation point. Until
        // it is not reached, do not skip instructions
        if (fetchPC == synchro_addr && !synchro_reached) {
                synchro_exec--;
                DPRINTF(NopsSimpleCPUExec, "Entry point reached\n");
            if ( synchro_exec <= 0 ) {
                synchro_reached = true;
                DPRINTF(NopsSimpleCPUExec, "Start skipping\n");
            } else {
                DPRINTF(NopsSimpleCPUExec, "%d execution(s) remaining\n",
                        synchro_exec);
            }
        }

        if (!synchro_reached) {
            TimingSimpleCPU::fetch();
            return;
        }

        // From here, we've executed the right number of time the address
        // specified in synchro_addr

        bool nop = false;

        // Find if the address to fetch is an address to skip
        std::map<Addr,
            std::queue<std::pair<char, long long int>*>*>::iterator it;
        it = skip_list.find(fetchPC);

        // If true, there is something to do with this address
        if ( it != skip_list.end() ) {

            // No specific action remaining, just skip
            if ( it->second->empty() ) {
                DPRINTF(NopsSimpleCPUExec, "Always skip 0x%x\n", fetchPC);
                nop = true;
            } else {
                // If true, "skip" action may be required
                if ( it->second->front()->first == 's' ) {
                    // If true, skip
                    if ( it->second->front()->second > 0 ) {
                        it->second->front()->second--;
                        nop = true;
                        DPRINTF(NopsSimpleCPUExec,
                                "Conditional skip 0x%x, %d remaining \n",
                                fetchPC,
                                it->second->front()->second);
                    } else {
                        nop = false;
                    }
                }
                // If true, do not skip
                else if ( it->second->front()->first == 'e') {
                    if ( it->second->front()->second > 0 ) {
                        it->second->front()->second--;
                        DPRINTF(NopsSimpleCPUExec,
                                "Conditional execute 0x%x, %d remaining\n",
                                fetchPC,
                                it->second->front()->second);
                    } else {
                        nop = true;
                    }
                }

                // Check if this action is over. If yes, then remove the action
                // from the queue.  Note that it does not depend on the action.
                // When counter reaches zero, it's done.
                //
                // If this is the last action, removal depends of the nature
                // of the action. If the last action is an execute, it means
                // that the next action should be a skip. Then we pop the
                // action since skipping is the default behavior if there is no
                // specific action to do.
                // However, if the last action is a skip, it means that the
                // next action sould be an execute. Hence, we do not pop the
                // last action and leave the counter at zero. Instruction will
                // be always executed.
                if ( it->second->front()->second == 0 ) {
                    if ( it->second->size() > 1 ) {
                        it->second->pop();
                    } else if ( it->second->front()->first == 'e' ) {
                        it->second->pop();
                    }
                }
            }
        }

        // If true, advance CPU state without sending the fetch request.
        // Otherwise call the normal fetch().
        if (nop) {
            _status = IcacheWaitResponse;
            completeIfetch(NULL);

            // TODO: does not seem mandatory, final tick is the same
            //TimingSimpleCPU::updateCycleCounts();
            updateCycleCounters(BaseCPU::CPU_STATE_ON);
        } else {
            TimingSimpleCPU::fetch();
        }
    }
    else {
        TimingSimpleCPU::fetch();
    }
}

// Source : http://www.cplusplus.com/forum/general/30929
unsigned int
NopsSimpleCPU::count_words(const std::string& s)
{
    std::string x = s;
    std::replace_if(x.begin(), x.end(),
            std::ptr_fun<int, int> (std::isspace), ' ');
    x.erase( 0, x.find_first_not_of( " " ) );

    if (x.empty()) {
        return 0;
    }

    return std::count(x.begin(), std::unique(x.begin(), x.end()), ' ') +
           !std::isspace(*s.rbegin());
}

std::string
NopsSimpleCPU::split_str(std::string line, unsigned int index) {
    return split(line, std::ptr_fun <int, int>(std::isspace)).at(index);
}

// Source : http://www.cplusplus.com/forum/general/30929
template <typename Predicate>
std::vector <std::string>
NopsSimpleCPU::split(const std::string& s, Predicate p)
{
    std::vector <std::string> result;
    std::string::const_iterator rtok;
    std::string::const_iterator ltok = std::find_if(s.begin(), s.end(),
            std::not1(p));

    while (ltok != s.end()) {
        rtok = std::find_if(ltok, s.end(), p);
        result.push_back(std::string(ltok, rtok));
        ltok = std::find_if(rtok, s.end(), std::not1(p));
    }

    return result;
}

////////////////////////////////////////////////////////////////////////
//
//  NopsSimpleCPU Simulation Object
//
NopsSimpleCPU *
NopsSimpleCPUParams::create()
{
    return new NopsSimpleCPU(this);
}

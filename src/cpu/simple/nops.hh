/*
 * Copyright (c) 2020, French Institute for Research in Computer Science and
 * Automation
 * Author: Pierre-Yves Peneau <first.last@inria.fr>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __CPU_SIMPLE_NOPS_HH__
#define __CPU_SIMPLE_NOPS_HH__

#include "cpu/simple/timing.hh"
#include "params/NopsSimpleCPU.hh"

class NopsSimpleCPU : public TimingSimpleCPU
{
  public:

    NopsSimpleCPU(NopsSimpleCPUParams *p);
    virtual ~NopsSimpleCPU();

    void fetch() override;

  private:

    bool synchro_reached;
    Addr synchro_addr; // Which address is the synchronisation point
    int  synchro_exec; // How many times do we have to execute this instruction

    // This map is main data structure.
    // Each instruction to skip is identified by its address and a queue
    // containing the actions on this specific instuction. Action are always
    // performed in a FIFO order.
    // Actions are specified using a std::pair<char, long long int>, where the
    // char represents the action and the integer the number of time we have to
    // perform this action. Once this counter reaches zero, the action is
    // removed from the FIFO and the next one (if any) will be executed
    // Actions can be:
    //   s: skip
    //   e: execute
    // followed by a number, e.g.:
    //   s:10 e:2
    // means "skip 10 times then execute 2 times"
    //
    // When there is no action remaining action, behavior depends of the nature
    // of the last action:
    //  * last action was a skip     -> always execute from now
    //  * last action was an execute -> always skip from now
    //
    // std::map<> provides a research time in O(1) on average, and O(n) at max
    // std::queue<> is the normal structure for a FIFO
    // std::pair<> is the easiest way to store two informations
    std::map<Addr, std::queue<std::pair<char, long long int>*>*> skip_list;

    std::string nopsFile;

    unsigned int count_words(const std::string& s);
    std::string split_str(std::string line, unsigned int index);
    template <typename Predicate> std::vector <std::string>
        split( const std::string& s, Predicate p );

};

#endif // __CPU_SIMPLE_NOPS_HH__
